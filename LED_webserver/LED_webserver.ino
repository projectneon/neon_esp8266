#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stddef.h>
#include <limits.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Adafruit_NeoPixel.h>

const char* ssid = "WLAN_AP_NAME";
const char* password = "WLAN_AP_PASSWORD";

#define PIXEL_PIN    14    // Digital IO pin connected to the NeoPixels.
#define PIXEL_COUNT 32

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);
char str[100];

MDNSResponder mdns;

ESP8266WebServer server(80);

const int led = 13;
bool manual_control = false;

void handleRoot() {
  digitalWrite(led, 1);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(led, 0);
}

void handleNotFound() {
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}

int target_r;
int target_g;
int target_b;
int curr_r;
int curr_g;
int curr_b;

int disp_r[PIXEL_COUNT];
int disp_g[PIXEL_COUNT];
int disp_b[PIXEL_COUNT];

void drive_led() {
  int val;
  digitalWrite(led, 1);

  val = server.arg(0).toInt();
  if (server.argName(0) == "r") {
    Serial.print("Red ");
    target_r = val;
  } else if (server.argName(0) == "g") {
    Serial.print("Green ");
    target_g = val;
  } else if (server.argName(0) == "b") {
    Serial.print("Blue ");
    target_b = val;
  }
  Serial.println(val);
  //setPixelColor(r,g,b);
  digitalWrite(led, 0);
  server.send(200, "text/plain", "OK");
}

int led_target(int curr, int target)
{
  if (curr == target) {
    return curr;
  }
  if (curr < target) {
    return curr + 1;
  }
  if (curr > target) {
    return curr - 1;
  }
}

void handleLed()
{
  int i;
  static int last_updated = 0;

  if (manual_control = false) {

    if (last_updated < millis()) {
      last_updated = millis() + 10;

      curr_r = led_target(curr_r, target_r);
      curr_g = led_target(curr_g, target_g);
      curr_b = led_target(curr_b, target_b);

      for (i = 1; i < PIXEL_COUNT; i++) {
        strip.setPixelColor(i - 1, strip.getPixelColor(i));
      }
      setPixelColor(curr_r, curr_g, curr_b);
      strip.show();
    } else {



    }
  }
}

void updateLed()
{
  int i;
  for (i = 0; i < PIXEL_COUNT; i++) {
      strip.setPixelColor(i, strip.Color(disp_r[i], disp_g[i], disp_b[i]));
  }
  strip.show();

}

void setPixelColor(int r, int g, int b) {
  strip.setPixelColor(PIXEL_COUNT - 1, strip.Color(r, g, b));
}

void setup(void) {
  int i;
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (mdns.begin("esp8266", WiFi.localIP())) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);
  server.on("/led", drive_led);
  server.on("/manual", handleManualLed);
  server.on("/all", handleAllLed);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  for(i=0;i<PIXEL_COUNT;i++){
    disp_r[i] = 0x00;
    disp_g[i] = 0x00;
    disp_b[i] = 0x10;
  }
  updateLed();
}

int target_led;
void handleManualLed()
{
  int val;
  char num[16];
  char* e;

  target_led = server.arg("l").toInt();  
    server.arg("rgb").toCharArray(num,15);
    val = strtol_replace(num, &e, 16);
    //sprintf(str,"Target LED = %d,RGB = %08Xh(str num=%s)\r\n", target_led, val, num);
    //Serial.print(str);
    disp_r[target_led] = (val >> 16) & 0xff;
    disp_g[target_led] = (val >>  8) & 0xff;
    disp_b[target_led] = (val >>  0) & 0xff;
  //setPixelColor(r,g,b);
  digitalWrite(led, 0);
  server.send(200, "text/plain", "OK");
  updateLed();
}


void handleAllLed()
{
  int val;
  char num[16];
  char* e;
  int rgb_color;
  int i;
  char arg_str[10];

  
  for(i=0;i<PIXEL_COUNT;i++){
    sprintf(arg_str,"l%d",i);
    server.arg(arg_str).toCharArray(num,16);
    rgb_color = strtol_replace(num, NULL, 16);
    disp_r[i] = (rgb_color >> 16) & 0xff;
    disp_g[i] = (rgb_color >>  8) & 0xff;
    disp_b[i] = (rgb_color >>  0) & 0xff;
  }
  server.send(200, "text/plain", "OK");
  updateLed();
}

void loop(void) {
  server.handleClient();
  updateLed();
}

/**
 * strtol() and strtoul() implementations borrowed from newlib:
 * http://www.sourceware.org/newlib/
 *      newlib/libc/stdlib/strtol.c
 *      newlib/libc/stdlib/strtoul.c
 *
 * Adapted for ESP8266 by Kiril Zyapkov <kiril.zyapkov@gmail.com>
 *
 * Copyright (c) 1990 The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the University of
 *    California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

long strtol_replace(const char *nptr, char **endptr, int base) {
    const unsigned char *s = (const unsigned char *)nptr;
    unsigned long acc;
    int c;
    unsigned long cutoff;
    int neg = 0, any, cutlim;

    /*
     * Skip white space and pick up leading +/- sign if any.
     * If base is 0, allow 0x for hex and 0 for octal, else
     * assume decimal; if base is already 16, allow 0x.
     */
    do {
        c = *s++;
    } while (isspace(c));
    if (c == '-') {
        neg = 1;
        c = *s++;
    } else if (c == '+')
        c = *s++;
    if ((base == 0 || base == 16) &&
        c == '0' && (*s == 'x' || *s == 'X')) {
        c = s[1];
        s += 2;
        base = 16;
    }
    if (base == 0)
        base = c == '0' ? 8 : 10;

    /*
     * Compute the cutoff value between legal numbers and illegal
     * numbers.  That is the largest legal value, divided by the
     * base.  An input number that is greater than this value, if
     * followed by a legal input character, is too big.  One that
     * is equal to this value may be valid or not; the limit
     * between valid and invalid numbers is then based on the last
     * digit.  For instance, if the range for longs is
     * [-2147483648..2147483647] and the input base is 10,
     * cutoff will be set to 214748364 and cutlim to either
     * 7 (neg==0) or 8 (neg==1), meaning that if we have accumulated
     * a value > 214748364, or equal but the next digit is > 7 (or 8),
     * the number is too big, and we will return a range error.
     *
     * Set any if any `digits' consumed; make it negative to indicate
     * overflow.
     */
    cutoff = neg ? -(unsigned long)LONG_MIN : LONG_MAX;
    cutlim = cutoff % (unsigned long)base;
    cutoff /= (unsigned long)base;
    for (acc = 0, any = 0;; c = *s++) {
        if (isdigit(c))
            c -= '0';
        else if (isalpha(c))
            c -= isupper(c) ? 'A' - 10 : 'a' - 10;
        else
            break;
        if (c >= base)
            break;
               if (any < 0 || acc > cutoff || (acc == cutoff && c > cutlim))
            any = -1;
        else {
            any = 1;
            acc *= base;
            acc += c;
        }
    }
    if (any < 0) {
        acc = neg ? LONG_MIN : LONG_MAX;
        errno = ERANGE;
    } else if (neg)
        acc = -acc;
    if (endptr != 0)
        *endptr = (char *) (any ? (char *)s - 1 : nptr);
    return (acc);
}

